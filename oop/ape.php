
<?php

class Ape extends Animal
{
  public $legs = 2;

  public function get_legs()
  {
     return $this->legs;
  }

  function yell()
  {
    return "Auooo";
  }
}

?>