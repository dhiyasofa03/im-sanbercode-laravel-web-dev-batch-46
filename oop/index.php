<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');


$sheep = new Animal("shaun");
echo "name: " . $sheep->name . "<br>"; // "shaun"
echo "legs: " . $sheep->legs . "<br>"; // 4
echo "cold blooded: " . $sheep->cold_blooded . "<br>"; // "no"
echo "<br>";

$kodok = new Frog("buduk");
echo "name: " . $kodok->name . "<br>";
echo "legs: " . $kodok->legs . "<br>";
echo "cold blooded: " . $kodok->cold_blooded . "<br>"; // "no"
echo "Jump: " . $kodok->jump() . "<br>"; // "hop hop"
echo "<br>";

$sungokong = new Ape("kera sakti");
echo "name: " . $sungokong->name . "<br>";
echo "legs: " . $sungokong->get_legs() . "<br>";
echo "cold blooded: " . $sungokong->cold_blooded . "<br>"; // "no"
echo "yell: " . $sungokong->yell(); // "Auooo"

